define({
    load_data: function (data_files, callback, data) {
        var base = this, data_array = data || [];
        if (data_files.length) {
            var data_file = data_files.shift();
            $.getJSON('data/' + data_file + ".json?" + new Date().getTime(), function (data) {
                data_array.push(data);
                base.load_data(data_files, callback, data_array);
            });
        } else {
            callback.apply(null, data_array);
        }
    },
    load_templates: function (templates, callback) {
        var base = this;
        if (templates.length) {
            var template = templates.pop();
            $.get('templates/' + template + ".moustache?" + new Date().getTime(), function (data) {
                ich.addTemplate(template, data);
                base.load_templates(templates, callback);
            });
        } else {
            callback.apply(null);
        }
    }
});