require([
    "lib/zepto",
    "lib/ICanHaz",
    "lib/bix",
    "utils/loaders",
    "models/dialogues"
], function($, ich, bix_loaded, util, dialogue) {
    util.load_data(["options", "conversations"], function (o, c) {
        util.load_templates(["talk_box", "talk_options"], function () {
            var convo = new Conversation();

            var bix = Bix({
                '/': function () {
                    location.hash = "#/dialogue/0";
                },
                '/dialogue/:id': function (id) {
                    convo.active("dialogue");
                    convo.current_dialogue(id);
                },
                '/option/:id': function (id) {
                    convo.active("option");
                    convo.current_options(id);
                },
                '/play': function () {
                    convo.view_dialogue();
                },
                '/demo': function () {
                    convo.load_convo(c, o);
                }
            });

            bix.config({
                forceHash: true,
                hashBang: false
            });

            bix.run();

            ko.applyBindings(convo);
        });
    });
});
