define(function () {
    var showText = function (target, message, interval, index) {
        index = index || 0;
        if (index < message.length) {
            $(target).append(message[index++]);
            setTimeout(function () { showText(target, message, interval, index); }, interval);
        }
    }

    return function (data) {
        var ConvoRunner = function (data) {
            this.data = data;
            this.set_convo(0);
        };

        ConvoRunner.prototype.set_convo = function (convo) {
            var self = this, co = this.data.conversations[convo];
            $('#opts, #convo').html('');
            this.convo = co.convo.slice();
            this.options = this.data.options[co.choices];
            this.next_convo();
            var button = $('<button>');
            button.text("next");
            button.click(function () {
                self.next_convo();
            })
            $('#opts').append(button);
        };

        ConvoRunner.prototype.next_convo = function () {
            var self = this;
            if (this.convo.length > 0) {
                var convo_line = this.convo.shift();
                $('#convo').html(ich.talk_box(convo_line));
                showText("#convo_play .chat", convo_line.dialogue, 50);
            } else {
                //clearInterval(self.intervalId);
                this.intervalId = null;
                if (this.options && this.options.options) {
                    $('#opts').html(ich.talk_options({options: self.options.options}));
                    $('.chat_option').click(function () {
                        self.set_convo($(this).data('convo'));
                    });
                }
            }
        };

        return new ConvoRunner(data);
    };
});
