define(["require", "../controllers/convo_run"], function (require, ConvoRun) {
    DialogueLine = function () {
        this.person = ko.observable();
        this.dialogue = ko.observable();
    };

    Dialogue = function (label) {
        var self = this;
        this.convo = ko.observableArray([new DialogueLine()]);
        this.choices = ko.observable(0);
        this.label = ko.observable(label || "new_dialogue");
        this.add_dialogue = function () {
            self.convo.push(new DialogueLine());
        };
        this.remove_dialogue = function (dialogue) {
            self.convo.remove(dialogue);
        };
        
    };

    OptionLine = function () {
        this.text = ko.observable();
        this.convo = ko.observable();
    };

    Options = function (label) {
        var self = this;
        this.label = ko.observable(label);
        this.options = ko.observableArray();
        this.add_option = function () {
            self.options.push(new OptionLine());
        };
        this.remove_option = function (option) {
            self.options.remove(option);
        };
    };

    Conversation = function () {
        var self = this;
        this.cr = false;
        this.conversations = ko.observableArray([new Dialogue("start")]);
        this.options = ko.observableArray([new Options("first_choices")]);
        this.current_options = ko.observable(0);
        this.current_dialogue = ko.observable(0);
        this.active = ko.observable("dialogue");

        this.get_dialogue = ko.computed(function () {
            return self.conversations()[self.current_dialogue()];
        });

        this.get_option = ko.computed(function () {
            return self.options()[self.current_options()];
        });

        this.set_dialogue = function (dialogue) {
            var index = self.conversations().indexOf(dialogue);
            location.hash = '#/dialogue/' + index;
        };

        this.add_dialogue = function () {
            var label = prompt("What to label this Dialogue as?");
            if(label){
                self.conversations.push(new Dialogue(label));
                location.hash = '#/dialogue/' + (self.conversations().length - 1);
            }
        };

        this.remove_dialogue = function () {
            var dia = self.get_dialogue();
            self.current_dialogue(0);
            self.conversations.remove(dia);
            location.hash = '#/dialogue/0';
        };

        this.set_option = function (option) {
            var index = self.options().indexOf(option);
            location.hash = '#/option/' + index;
        };

        this.add_option = function () {
            var label = prompt("What to label this Option as?");
            if(label){
                self.options.push(new Options(label));
                location.hash = '#/option/' + (self.options().length - 1);
            }
            
        };
        
        this.remove_option = function () {
            var opt = self.get_option();
            self.current_options(0);
            self.options.remove(opt);
            location.hash = '#/option/0';
        };

        this.view_dialogue = function () {
            self.active('play');
            self.cr = ConvoRun(ko.toJS(self));
        };

        this.play = function () {
            location.hash = '#/play';
        };

        this.load_convo = function (convo, options) {
            var i, o_len = options.length, c_len = convo.length;
            self.options([]);
            for (i = 0; i < o_len; i++) {
                var option = ko.mapping.fromJS(options[i], {}, new Options());
                self.options.push(option);
            }
            self.conversations([]);
            for (i = 0; i < c_len; i++) {
                var conv = ko.mapping.fromJS(convo[i], {}, new Dialogue());
                self.conversations.push(conv);
            }
        };
    };
});
